import React from "react";
import { ReactDOM } from "react";
import { Switch, Route, Link, BrowserRouter, history } from 'react-router-dom'
import { Grid, Select, Input, MenuItem, TextField, Button, Typography, Box } from '@material-ui/core';
import { makeStyles, theme, withStyles } from "@material-ui/core/styles";
import { Redirect } from "react-router";
import Paper from "@material-ui/core/Paper";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import ViewAssets from "./viewAssets";
import CreateAsset from "./createAsset";
import AssetData from "./assetData";
import Login from "./login";

function NavBar() {
    const routes = ["/viewAsset", "/createAsset"];
    return (
        <React.Fragment>
            <Route

                path="/"
                render={(history) => (
                    <Tabs
                        value={
                            history.location.pathname !== "/"
                                ? history.location.pathname
                                : false
                        }
                    >
                        {console.log(history.location.pathname)}
                        <Tab
                            value={routes[0]}
                            label="View assets"
                            component={Link}
                            to={routes[0]}
                        />
                        <Tab
                            value={routes[1]}
                            label="Create assets"
                            component={Link}
                            to={routes[1]}
                        />
                    </Tabs>

                )}
            />

            <Switch>
                <Route path="/viewAsset" component={ViewAssets} />
                <Route path="/createAsset" component={CreateAsset} />
            </Switch>
            </React.Fragment>
    )
}

export default NavBar