import React, { useEffect, useState } from "react";
import reactDom from "react-dom";
import { Grid, TextField, Button, Backdrop, CircularProgress, Card, Paper } from '@material-ui/core';
import { useHistory, Redirect } from "react-router-dom";

export default function Login(props) {
    const [state, setState] = useState({
        userid: "",
        password: ""
    });

    const [error, setError] = useState({
        message: ""
    })

    const handleChange = (e) => {
        const { id, value } = e.target
        setState(prevState => ({
            ...prevState,
            [id]: value
        }))
    }

    const history = useHistory();

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(state);
        if (state.userid === '' || state.password === '') {
            setError({ message: "User id and password should not be blank" });
            return;
        }
        localStorage.setItem('token', state.userid);
        console.log(history);
        history.push('/viewAsset');
    }
    return (
        <div style={{ textAlign: "center" }}>
            
                <form>
                <Paper elevation={3}>
                    <Grid container>
                        <Grid item xs={12} sm={12} md={12}>
                            <nav className="navbar navbar-dark bg-primary">
                                <div style={{ textAlign: "center", fontSize: "larger", fontWeight: "bold" }}>
                                     Login </div>
                            </nav>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={12} sm={12} md={12}>
                            <TextField
                                id="userid"
                                value={state.userid}
                                onChange={handleChange}
                                //InputProps={{ classes: { input: classes.input } }}
                                variant="standard"
                                className="text-input"
                                style={{ marginTop: '10px' }}
                                label="User id"
                                margin="normal" />
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={12} sm={12} md={12}>
                            <TextField
                                id="password"
                                value={state.password}
                                onChange={handleChange}
                                //InputProps={{ classes: { input: classes.input } }}
                                type="password"
                                variant="standard"
                                className="text-input"
                                style={{ marginTop: '10px' }}
                                label="Password"
                                margin="normal" />
                        </Grid>
                    </Grid>
                    <div style={{ color: "red", marginTop: '20px' }}>{error.message}</div>
                    <Grid container spacing={1}>
                        <Grid item xs={12} md={12} style={{ textAlign: 'center' }}>
                            <Button justify="center" variant="outlined" color="primary"
                                style={{ backgroundColor: "blue", color: "white", marginTop: '30px', alignItems: 'center' }}
                                onClick={handleSubmit}
                            > Login </Button>
                        </Grid>
                    </Grid>
                    </Paper>
                </form>
            
        </div>
    )
}