import React from "react";
import PropTypes from 'prop-types';
import { alpha } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import {visuallyHidden} from '@mui/utils';
import {DataGrid} from '@material-ui/data-grid'
import { makeStyles } from "@material-ui/core";
  
  
  const columns = [
    { field: 'afour_gadget_id', headerName: 'Gadget Id', width: 150, flex: 1 },
    { field: 'asset_category_id', headerName: 'Category', width: 150, flex: 1},
    { field: 'gadget_type', headerName: 'Gadget Type', width: 160, flex: 1 },
    {
      field: 'configuration',
      headerName: 'Configuration',
      width: 170, flex: 1
    },
    {
      field: 'device_name',
      headerName: 'Device',
      description: 'This is the device name',
      sortable: false,
      width: 120,
      flex: 1
    },
    {
        field: 'serial_number',
        headerName: 'Serial No',
        width: 150,flex: 1
      },
      {
        field: 'isAssigned',
        headerName: 'Assigned',
        width: 150,flex: 1
      },
  ];
  
  const rows = [
    {
        "afour_gadget_id": 1,
        "asset_category_id": 123,
        "gadget_type": "laptop",
        "configuration": "ubuntu-18",
        "license_id": 1,
        "allocation_date": "1990-06-20T08:03:00",
        "asset_id": 1,
        "sys_admin_id": 1,
        "device_name": "lenovo",
        "serial_number": "A4-123",
        "allocation_id": 1,
        "isAssigned": true
      },
      {
        "afour_gadget_id": 1,
        "asset_category_id": 123,
        "gadget_type": "laptop",
        "configuration": "ubuntu-18",
        "license_id": 1,
        "allocation_date": "1990-06-20T08:03:00",
        "asset_id": 1,
        "sys_admin_id": 1,
        "device_name": "lenovo",
        "serial_number": "A4-123",
        "allocation_id": 1,
        "isAssigned": true
      },
      {
        "afour_gadget_id": 1,
        "asset_category_id": 123,
        "gadget_type": "laptop",
        "configuration": "ubuntu-18",
        "license_id": 1,
        "allocation_date": "1990-06-20T08:03:00",
        "asset_id": 1,
        "sys_admin_id": 1,
        "device_name": "lenovo",
        "serial_number": "A4-123",
        "allocation_id": 1,
        "isAssigned": true
      },
      {
        "afour_gadget_id": 1,
        "asset_category_id": 123,
        "gadget_type": "laptop",
        "configuration": "ubuntu-18",
        "license_id": 1,
        "allocation_date": "1990-06-20T08:03:00",
        "asset_id": 1,
        "sys_admin_id": 1,
        "device_name": "lenovo",
        "serial_number": "A4-123",
        "allocation_id": 1,
        "isAssigned": true
      },
      {
        "afour_gadget_id": 1,
        "asset_category_id": 123,
        "gadget_type": "laptop",
        "configuration": "ubuntu-18",
        "license_id": 1,
        "allocation_date": "1990-06-20T08:03:00",
        "asset_id": 1,
        "sys_admin_id": 1,
        "device_name": "lenovo",
        "serial_number": "A4-123",
        "allocation_id": 1,
        "isAssigned": true
      },
      {
        "afour_gadget_id": 2,
        "asset_category_id": 321,
        "gadget_type": "laptop",
        "configuration": "ubuntu-18",
        "license_id": 2,
        "allocation_date": "1991-06-20T08:03:00",
        "asset_id": 2,
        "sys_admin_id": 2,
        "device_name": "lenovo",
        "serial_number": "A4-123",
        "allocation_id": 2,
        "isAssigned": true
      }
  ].map((e,i) => {
    return {
        ...e, id: i
    }
});

const useStyles = makeStyles({
  dataGrid: {
    background: "linear-gradient(45deg, lightblue, 30%, white 80%)",
    borderRadius: 3,
    border: 1,
    color: "black",
    height: "100%",
    padding: "0 30px",
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    width: "100%"
  }
});
  
  export default function DataTable() {
    const classes = useStyles();
    return (
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid
          rows={rows}
          columns={columns}
          pageSize={5}
          rowsPerPageOptions={[5]}
          checkboxSelection
          className={classes.dataGrid}
        />
      </div>
    );
  }

