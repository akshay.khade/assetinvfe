import React, {useEffect, useState} from "react";
import reactDom from "react-dom";
import { Grid, TextField, Button, Select, Backdrop, CircularProgress, Card, MenuItem, InputLabel, FormControl, FormHelperText } from '@material-ui/core';
import { MuiPickersUtilsProvider, DatePicker } from "material-ui-pickers";
import DateFnsUtils from "@date-io/date-fns/build";

export default function CreateAsset() {
    const year = new Date().getFullYear().toString();
    const [org, setOrg] = React.useState("");
    const [asset, setAsset] = React.useState("");
    const [approver, setApprover] = React.useState("");
    const [allocationDate, setAllocationDate] = React.useState(null);
    const [orgError, setorgError] = React.useState("");
    const [assetError, setassetError] = React.useState("");
    const [approverError, setapproverError] = React.useState("");
    const [inputFields , setInputFields] = React.useState({
        organization:'',
        assettype:'',
        approver:'',
        configuration:'',
        licenceid:'',
        serialno:'',
        device: ''
    })

    const assetCollection = [{
        "assetName": "Laptop",
        "assetCode": "LT"
    },
    {
        "assetName": "Monitor",
        "assetCode": "MT"
    },
    {
        "assetName": "Tablet",
        "assetCode": "TB"
    },
    {
        "assetName": "Desktop",
        "assetCode": "DT"
    },
    {
        "assetName": "Other asset",
        "assetCode": "OA"
    },
    ]

    const inputsHandler = (e) =>{
        setInputFields( {...inputFields,[e.target.name]: e.target.value} );
        console.log(inputFields);
    }

    const setDate = (e) => {
        setAllocationDate(e);
        console.log(allocationDate);
    }

    useEffect(() => {
        console.log(allocationDate);
      }, [allocationDate]);

    const submitHandler = (e) => {
        e.preventDefault();
        formValidate();
        let payload = {
            assetId: inputFields.organization + year.substring(year.length - 2) + inputFields.assettype + "000001",
            configuration: inputFields.configuration,
            license_id: inputFields.licenceid,
            serial_no: inputFields.serialno,
            device_name: inputFields.device,
            gadget_type: inputFields.assettype,
            allocation_date: allocationDate.toISOString()
        }
        console.log(payload)
    }

    const formValidate = () => {
        if (inputFields.organization === '') {
            setorgError('Select organization');
            return;
        }
        if (inputFields.assettype === '') {
            setassetError('Select an asset');
            return;
        }
        if (inputFields.approver === '') {
            setapproverError('Select approver');
            return;
        }
    }

    const styles = theme => ({
        datePickerStyle: {
            width: "200px",
            height: "40px",
            borderRadius: "25px"
        }
    })
    
    const classes = styles();

    return (
        <div style={{ textAlign: "center" }}>
            <form>
                <Grid container>
                    <Grid item xs={12} sm={12}>
                        <div style={{ fontWeight: "bold", fontSize: "larger", textAlign: "center" }}>
                            Create New Asset</div>
                    </Grid>
                </Grid>
                <hr />
                <Grid container>
                    <Grid item xs={12} md={4}>
                        <FormControl required sx={{ m: 1, minWidth: 120 }}>
                            <InputLabel id="demo-simple-select-standard-label">Organization</InputLabel>
                            <Select
                                labelId="demo-simple-select-standard-label"
                                id="organization"
                                name="organization"
                                value={inputFields.organization}
                                onChange={inputsHandler}
                                label="Organization*"
                                style={{ width: "200px" }}
                            >
                                <MenuItem value="A4">AFour</MenuItem>
                                <MenuItem value="RE">Rental</MenuItem>
                                <MenuItem value="CE">Client</MenuItem>
                            </Select>
                            <FormHelperText><div style={{ color: "red", marginTop: '20px' }}>{orgError}</div></FormHelperText>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <FormControl required sx={{ m: 1, minWidth: 120 }}>
                            <InputLabel id="demo-simple-select-standard-label">Asset type</InputLabel>
                            <Select
                                labelId="demo-simple-select-standard-label"
                                name="assettype"
                                value={inputFields.assettype}
                                onChange={inputsHandler}
                                label="Asset type"
                                style={{ width: "200px" }}
                            >{
                                    assetCollection.map((item) => (
                                        <MenuItem value={item.assetCode}>
                                            {item.assetName}
                                        </MenuItem>
                                    ))
                                }
                            </Select>
                            <FormHelperText><div style={{ color: "red", marginTop: '20px' }}>{assetError}</div></FormHelperText>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <TextField
                            name="configuration"
                            defaultValue=''
                            value={inputFields.configuration}
                            onChange={inputsHandler}
                            //InputProps={{ classes: { input: classes.input } }}
                            variant="standard"
                            className="text-input"
                            style={{ marginTop: '10px' }}
                            label="Configuration"
                            margin="normal" />
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={12} md={4}>
                        <TextField
                            name="licenceid"
                            value={inputFields.licenceid}
                            onChange={inputsHandler}
                            //InputProps={{ classes: { input: classes.input } }}
                            variant="standard"
                            className="text-input"
                            style={{ marginTop: '10px' }}
                            label="Licence id"
                            margin="normal" />
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <TextField
                            name="serialno"
                            value={inputFields.serialno}
                            onChange={inputsHandler}
                            //InputProps={{ classes: { input: classes.input } }}
                            variant="standard"
                            className="text-input"
                            style={{ marginTop: '10px' }}
                            label="Serial No"
                            margin="normal" />
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <TextField
                            name="device"
                            value={inputFields.device}
                            onChange={inputsHandler}
                            //InputProps={{ classes: { input: classes.input } }}
                            variant="standard"
                            className="text-input"
                            style={{ marginTop: '10px' }}
                            label="Device name"
                            margin="normal" />
                    </Grid>
                </Grid>
                <div style={{ marginTop: "5px" }}>
                    <Grid container>
                        <Grid item xs={12} md={4}>
                            <FormControl required sx={{ m: 1, minWidth: 120 }}>
                                <InputLabel id="demo-simple-select-standard-label">Approver</InputLabel>
                                <Select
                                    labelId="demo-simple-select-standard-label"
                                    name="approver"
                                    value={inputFields.approver}
                                    onChange={inputsHandler}
                                    label="Approver"
                                    style={{ width: "200px" }}
                                >
                                    <MenuItem value="satish.nikalje@afourtech.com">Satish Nikalje</MenuItem>
                                    <MenuItem value="deepak.kale@afourtech.com">Deepak Kale</MenuItem>
                                </Select>
                                <FormHelperText><div style={{ color: "red", marginTop: '20px' }}>{approverError}</div></FormHelperText>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <DatePicker
                                label="Allocation date"
                                    value={allocationDate}
                                    autoOk
                                    format="MM/dd/yyyy"
                                    variant="standard"
                                    maxDate={new Date()}
                                //className={classes.datePickerStyle}
                                onChange={setDate} />
                            </MuiPickersUtilsProvider>
                        </Grid>
                    </Grid>
                </div>
                <Grid container>

                </Grid>
                <Grid container spacing={1}>
                    <Grid item xs={12} md={12} style={{ textAlign: 'right' }}>
                        <Button justify="right" variant="outlined"
                            style={{ backgroundColor: "grey", color: "white", marginRight: "5px", marginTop: '30px', alignItems: 'center' }}
                        > Reset </Button>
                        <Button justify="right" variant="outlined" onClick={submitHandler}
                            style={{ backgroundColor: "blue", color: "white", marginTop: '30px', alignItems: 'center' }}
                        > Submit </Button>
                    </Grid>
                </Grid>
            </form>
        </div>
    )
}