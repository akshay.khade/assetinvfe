import React from 'react'
import {Route, Redirect} from 'react-router-dom'

const PrivateRoute = ({
    component: Component,
  	isAuthenticated,
  	userLevel,
    ...rest
}) => (
    <Route {...rest} component={(props)=> 
            isAuthenticated ? (
                <Component {...props} />
            ):(
                <Redirect to="/login" />
            )
    } />
)
export default PrivateRoute