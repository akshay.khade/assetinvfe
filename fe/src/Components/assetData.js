import React from "react";
import { ReactDOM } from "react";
import { Grid, Select, Input, MenuItem, TextField, Button, Typography, Box } from '@material-ui/core';
import { makeStyles, theme, withStyles } from "@material-ui/core/styles";
import { Redirect } from "react-router";
import Paper from "@material-ui/core/Paper";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import ViewAssets from "./viewAssets";
import CreateAsset from "./createAsset";
import { BrowserRouter, Route } from "react-router-dom";
import NavBar from "./navBar";

function AssetData() {
    const [value, setValue] = React.useState(0);
    const routes = ['/viewAsset', '/createAsset'];
    return (
        <div style={{ width: "80%", margin: "0 auto", height: "100%" }}>
            <NavBar/>
        </div>
    )
}

export default AssetData;
