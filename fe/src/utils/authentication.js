import React, { useEffect, useState } from "react";
import reactDom from "react-dom";
import { Grid, TextField, Button, Backdrop, CircularProgress, Card, Paper } from '@material-ui/core';
import { useHistory } from "react-router-dom";

export default function Authenticate() {
    const isAuthenticated = () => {
        const token = localStorage.getItem('token');
        try {
            if (token) {
                return true;
            }
            else {
                return false;
            }
        } catch (error) {
            return false;
        }
    }
}