import logo from './logo.svg';
import './App.css';
import { Switch, Route, BrowserRouter, Redirect } from 'react-router-dom';
import AssetData from './Components/assetData';
import Login from './Components/login';
import { Button, Grid } from '@material-ui/core';
import AfourLogo from './AfourLogo.png';
import NavBar from './Components/navBar';
import Authenticate from './utils/authentication';
import CreateAsset from './Components/createAsset';

const isAuthenticated = () => { 
  const token = localStorage.getItem('token');
try {
if(token){
  return true;
}
else{
  return false;
}
} catch (error) {
return false;
}
}

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated() ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
            }}
          />
        )
      }
    />
  );
}

function App() {
  

  return (
    <div style={{ height: '100%', textAlign: "center" }}>
      <img src={AfourLogo} alt="AfourLogo" />
      <h2 style={{ textAlign: "center", color: "blue" }}>AFour Asset Inventory Management</h2>
      {/* {if isAuthenticated && <Button id="btnLogOut" className="primary">Logout</Button>} */}
      <BrowserRouter>
      <Switch>
      <Route path="/login" exact component={Login}/>
      <PrivateRoute path="/" exact component={Login}/>
      <PrivateRoute path="/viewAsset" exact component={AssetData}/>
      <PrivateRoute path="/createAsset" exact component={AssetData}/>
      </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
